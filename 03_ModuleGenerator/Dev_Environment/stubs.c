/******************************************************************************
  * @file    stubs.c 
  * @author  Reinemuth Sebastian
  * @date    20-02-2018
  * @brief   stubs-Functions
  * last checkin :
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
  *****************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include "stubs.h"
#include "UartIf_LoIf.h"
#include "Std_Types.h"
/* Global variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Functions -----------------------------------------------------------------*/

/**
* @brief           API function to set a transmitt data frame
* @param[in]       UartIf_TxFrameType   Uart layer frame data
* @retval          Std_ReturnType
*                  >> E_OK
*                  >> E_NOT_OK
* @details         This function could be used to set a new transmit data frame from the uart module
*/
Std_ReturnType Uart_Transmit(UartIf_TxFrameType frameData)
{
}
