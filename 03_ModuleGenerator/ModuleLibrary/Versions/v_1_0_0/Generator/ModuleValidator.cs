﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleValidator
  {
    private Models.UartIfXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(Models.UartIfXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartOfValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, xmlCfg.xmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      /* Check if configuration file parameter is available */
      if(0 ==xmlCfg.miBMainfunctionCycleTimeMs)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }

      /* Validate all config modules */
      if (( null == xmlCfg.uartModuleCfgList ) || ( 0 == xmlCfg.uartModuleCfgList.Count ))
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg);
      }
      else
      {
        for (int idx = 0; idx < xmlCfg.uartModuleCfgList.Count; idx++)
        {
          if (0 == xmlCfg.uartModuleCfgList[idx].uartIfCntOfRxFrames)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg_CntRxFrames + idx.ToString());
          }

          if (0 == xmlCfg.uartModuleCfgList[idx].uartIfCntOfTxFrames)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg_CntTxFrames + idx.ToString());
          }

          if (0 == xmlCfg.uartModuleCfgList[idx].uartIfTxFrameDataSize)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg_TxFrameSize + idx.ToString());
          }

          if (0 == xmlCfg.uartModuleCfgList[idx].uartIfRxFrameDataSize)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg_RxFrameSize + idx.ToString());
          }

          if (null == xmlCfg.uartModuleCfgList[idx].uartRxNotificationFnc)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg_RxNotFnc + idx.ToString());
          }
          else if (( true == xmlCfg.uartModuleCfgList[idx].uartRxNotificationFnc.Contains(' ') ) ||
            ( false == System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.uartModuleCfgList[idx].uartRxNotificationFnc,
                       @"[A-Za-z\d_]") ))
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg_RxNotFnc + idx.ToString());
          }


          if (null == xmlCfg.uartModuleCfgList[idx].uartRxIsrNotificationFnc)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg_RxIsrNotFnc + idx.ToString());
          }
          else if (( true == xmlCfg.uartModuleCfgList[idx].uartRxIsrNotificationFnc.Contains(' ') ) ||
            ( false == System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.uartModuleCfgList[idx].uartRxIsrNotificationFnc,
                       @"[A-Za-z\d_]") ))
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg_RxIsrNotFnc + idx.ToString());
          }

          if (null == xmlCfg.uartModuleCfgList[idx].uartTxNotificationFnc)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg_TxNotFnc + idx.ToString());
          }
          else if (( true == xmlCfg.uartModuleCfgList[idx].uartTxNotificationFnc.Contains(' ') ) ||
            ( false == System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.uartModuleCfgList[idx].uartTxNotificationFnc,
                       @"[A-Za-z\d_]") ))
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg_TxNotFnc + idx.ToString());
          }

          if (null == xmlCfg.uartModuleCfgList[idx].uartTxIsrNotificationFnc)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg_TxIsrNotFnc + idx.ToString());
          }
          else if (( true == xmlCfg.uartModuleCfgList[idx].uartTxIsrNotificationFnc.Contains(' ') ) ||
            ( false == System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.uartModuleCfgList[idx].uartTxIsrNotificationFnc,
                       @"[A-Za-z\d_]") ))
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg_TxIsrNotFnc + idx.ToString());
          }
        }
      }

      info.AddLogMsg(ValidateResources.LogMsg_ValidationDone);
      return info;
    }
  }
}
