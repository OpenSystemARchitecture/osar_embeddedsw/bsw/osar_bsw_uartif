﻿/*****************************************************************************************************************************
 * @file        Module_ViewModel.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Module Xml Cfg Class                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;
using System.IO;
using OsarResources.Generator;

using System.Collections.ObjectModel;
using System.ComponentModel;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// Delegate to store the Configuration
  /// </summary>
  public delegate void StoreConfiguration();

  public class Module_ViewModel : BaseViewModel, OsarModuleGeneratorInterface
  {
    private static Models.UartIfXml UartIfXmlCfg = new Models.UartIfXml();
    private static string pathToConfiguratioFile;
    private static string pathToModuleBaseFolder;
    private static StoreConfiguration storeCfg = new StoreConfiguration(SaveConfigToXml);

    private static ObservableCollection<UartModuleCfgVM> uartModuleCfgObservableCollection;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Should be a absolute Path</param>
    /// <param name="absPathToBaseModuleFolder"> Should be a absolute Path</param>
    public Module_ViewModel(string pathToCfgFile, string absPathToBaseModuleFolder) : base(storeCfg)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      /* Read Configuration File */
      if (!File.Exists(pathToConfiguratioFile))
      {
        // If config file does not exist, create a default file
        SetDefaultConfiguration();
      }

      /* Read Configuration File */
      ReadConfigFromXml();

      // Initialize Uart module list
      uartModuleCfgObservableCollection = new ObservableCollection<UartModuleCfgVM>();
      for (int idx = 0; idx < UartIfXmlCfg.uartModuleCfgList.Count; idx++)
      {
        uartModuleCfgObservableCollection.Add(new UartModuleCfgVM(UartIfXmlCfg.uartModuleCfgList[idx], storeCfg));
      }
    }


    #region public attributes
    /// <summary>
    /// Interface for the XML File Version
    /// </summary>
    public XmlFileVersion XmlFileVersion
    {
      get { return UartIfXmlCfg.xmlFileVersion; }
      set { UartIfXmlCfg.xmlFileVersion = value; }
    }

    /// <summary>
    /// Interface for the XML File Version string
    /// </summary>
    public string XmlFileVersion_String
    {
      get
      {
        return ( "v." + UartIfXmlCfg.xmlFileVersion.MajorVersion.ToString() + "." +
          UartIfXmlCfg.xmlFileVersion.MinorVersion.ToString() + "." +
          UartIfXmlCfg.xmlFileVersion.PatchVersion.ToString() );
      }
    }

    /// <summary>
    /// Interface for the Det Module Id
    /// </summary>
    public UInt16 DetModuleID
    {
      get { return UartIfXmlCfg.detModuleID; }
      set { UartIfXmlCfg.detModuleID = value; }
    }

    /// <summary>
    /// Interface for the Det Module Usage
    /// </summary>
    public SystemState DetModuleUsage
    {
      get { return UartIfXmlCfg.detModuleUsage; }
      set { UartIfXmlCfg.detModuleUsage = value; }
    }

    /// <summary>
    /// Interface for the Module Version
    /// </summary>
    public string ModuleVersion
    {
      get { return Generator.DefResources.ModuleVersion; }
    }

    /// <summary>
    /// Interface for the miBMainfunctionCycleTimeMs
    /// </summary>
    public UInt16 MainfunctionCycleTime
    {
      get => UartIfXmlCfg.miBMainfunctionCycleTimeMs;
      set => UartIfXmlCfg.miBMainfunctionCycleTimeMs = value;
    }

    /// <summary>
    /// Interface for the uartModuleCfgObservableCollection
    /// </summary>
    public static ObservableCollection<UartModuleCfgVM> UartModuleCfgList
    {
      get => uartModuleCfgObservableCollection;
      set => uartModuleCfgObservableCollection = value;
    }
    #endregion

    #region UI Action Interface
    /// <summary>
    /// Interface to add a new uart configuration
    /// </summary>
    public void AddNewUartCfg()
    {
      uartModuleCfgObservableCollection.Add(new UartModuleCfgVM(new Models.uartModuleCfg(), storeCfg));
      OnPropertyChanged(new PropertyChangedEventArgs("UartModuleCfgList"));
    }

    /// <summary>
    /// Interface to remove a uart configuration
    /// </summary>
    /// <param name="uartCfg"></param>
    public void RemoveUartCfg(UartModuleCfgVM uartCfg)
    {
      uartModuleCfgObservableCollection.Remove(uartCfg);
      OnPropertyChanged(new PropertyChangedEventArgs("UartModuleCfgList"));
    }
    #endregion

    #region Configuration Load and Storage
    /*
     * @brief       Function to store the current configuration structure to an xml file
     * @param       none
     * @retval      none
     */
    private static void SaveConfigToXml()
    {
      // Write back UI page configuration data to config structure
      if (null != uartModuleCfgObservableCollection)
      {
        UartIfXmlCfg.uartModuleCfgList.Clear();
        foreach (var element in uartModuleCfgObservableCollection)
        {
          UartIfXmlCfg.uartModuleCfgList.Add(element.UartCfg);
        }
      }

      XmlSerializer writer = new XmlSerializer(UartIfXmlCfg.GetType());
      StreamWriter file = new StreamWriter(pathToConfiguratioFile);
      writer.Serialize(file, UartIfXmlCfg);
      file.Close();
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       none
     * @retval      none
     */
    private static void ReadConfigFromXml()
    {
      XmlSerializer reader = new XmlSerializer(UartIfXmlCfg.GetType());
      StreamReader file = new StreamReader(pathToConfiguratioFile);
      UartIfXmlCfg = ( Models.UartIfXml)reader.Deserialize(file);
      file.Close();
    }
    #endregion

    #region Generic Osar Generator
    /// <summary>
    /// Used to validate the active configuration of its correctness.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType ValidateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Ensure a correct version */
      if (null == UartIfXmlCfg)
      {
        genInfo1 = SetDefaultConfiguration();
      }

      /* Call Validator */
      Generator.ModuleValidator modVal = new Generator.ModuleValidator(UartIfXmlCfg, pathToConfiguratioFile);
      genInfo2 = modVal.ValidateConfiguration();

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);
      return genInfoMerge;
    }

    /// <summary>
    /// Used to generate the active configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType GenerateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Validate configuration */
      genInfo1 = ValidateConfiguration();

      /* If no validation error has been detected >> Start generation */
      if (0U == genInfo1.error.Count)
      {
        Generator.ModuleGenerator modGen = new Generator.ModuleGenerator(UartIfXmlCfg, pathToConfiguratioFile, pathToModuleBaseFolder);
        genInfo2 = modGen.GenerateConfiguration();
      }

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);

      return genInfoMerge;
    }

    /// <summary>
    /// Used to set the default configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType SetDefaultConfiguration()
    {
      GenInfoType genInfo;
      Generator.DefaultCfgGenerator defGen = new Generator.DefaultCfgGenerator(UartIfXmlCfg, pathToConfiguratioFile);
      genInfo = defGen.CreateDefaultConfiguration();
      SaveConfigToXml();
      return genInfo;
    }

    /// <summary>
    /// Interface to update an outdated configuration to an up to date configuration version
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType UpdateConfigurationFileVersion()
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(UartIfXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion();
      SaveConfigToXml();
      return genInfo;
    }
    /// <summary>
    /// Interface to update an outdated configuration to an specific configuration version
    /// </summary>
    /// <param name="updateToVersion"> Xml file version to be updated </param>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs</returns>
    public GenInfoType UpdateConfigurationFileVersion(XmlFileVersion updateToVersion)
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(UartIfXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion(updateToVersion);
      SaveConfigToXml();
      return genInfo;
    }
    #endregion
  }
}
