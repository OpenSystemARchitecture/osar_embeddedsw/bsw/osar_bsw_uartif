﻿/*****************************************************************************************************************************
 * @file        UartModuleCfgVM.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        28.09.2019                                                                                                   *
 * @brief       Implementation of the View Model of the Uart module configuration                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  public class UartModuleCfgVM : BaseViewModel
  {
    private uartModuleCfg uartCfg;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfg"></param>
    /// <param name="storeCfg"></param>
    public UartModuleCfgVM(uartModuleCfg cfg, StoreConfiguration storeCfg): base(storeCfg)
    {
      uartCfg = cfg;
    }

    /// <summary>
    /// Interface for Attribute uartCfg
    /// </summary>
    public uartModuleCfg UartCfg { get => uartCfg; set => uartCfg = value; }

    /// <summary>
    /// Interface for Attribute uartIfCntOfTxFrames
    /// </summary>
    public Byte UartIfCntTxFrames { get => uartCfg.uartIfCntOfTxFrames; set => uartCfg.uartIfCntOfTxFrames = value; }

    /// <summary>
    /// Interface for Attribute uartIfTxFrameDataSize
    /// </summary>
    public Byte UartIfTxFrameLength { get => uartCfg.uartIfTxFrameDataSize; set => uartCfg.uartIfTxFrameDataSize = value; }

    /// <summary>
    /// Interface for Attribute uartIfCntOfRxFrames
    /// </summary>
    public Byte UartIfCntRxFrames { get => uartCfg.uartIfCntOfRxFrames; set => uartCfg.uartIfCntOfRxFrames = value; }

    /// <summary>
    /// Interface for Attribute uartIfRxFrameDataSize
    /// </summary>
    public Byte UartIfRxFrameLength { get => uartCfg.uartIfRxFrameDataSize; set => uartCfg.uartIfRxFrameDataSize = value; }

    /// <summary>
    /// Interface for Attribute uartTxNotificationFnc
    /// </summary>
    public String UartTxNotificationFnc { get => uartCfg.uartTxNotificationFnc; set => uartCfg.uartTxNotificationFnc = value; }

    /// <summary>
    /// Interface for Attribute uartTxIsrNotificationFnc
    /// </summary>
    public String UartTxIsrNotificationFnc { get => uartCfg.uartTxIsrNotificationFnc; set => uartCfg.uartTxIsrNotificationFnc = value; }

    /// <summary>
    /// Interface for Attribute uartRxNotificationFnc
    /// </summary>
    public String UartRxNotificationFnc { get => uartCfg.uartRxNotificationFnc; set => uartCfg.uartRxNotificationFnc = value; }

    /// <summary>
    /// Interface for Attribute uartRxIsrNotificationFnc
    /// </summary>
    public String UartRxIsrNotificationFnc { get => uartCfg.uartRxIsrNotificationFnc; set => uartCfg.uartRxIsrNotificationFnc = value; }
  }
}
