﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;

namespace UartIfXsd
{
  public struct uartModuleCfg
  {
    public Byte uartIfCntOfTxFrames;
    public Byte uartIfTxFrameDataSize;
    public Byte uartIfCntOfRxFrames;
    public Byte uartIfRxFrameDataSize;
    public String uartRxNotificationFnc;
    public String uartRxIsrNotificationFnc;
    public String uartTxNotificationFnc;
    public String uartTxIsrNotificationFnc;
  }

  /**
    * @brief    UartIf module xsd class to generate an xml style sheet
    */
  public class UartIfXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public UInt16 miBMainfunctionCycleTimeMs;
    public List<uartModuleCfg> uartModuleCfgList;
  }
}
