/*****************************************************************************************************************************
* @file        UartIf_PBCfg.h                                                                                                 *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of definitions / interface function prototypes / datatypes for STUBS !!!!                     *
* @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UartIf_PBCfg_H
#define __UartIf_PBCfg_H

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*----------------------------------  Development error trace information for this module ----------------------------------*/
#define UARTIF_DET_MODULE_ID  1
#define UARTIF_MODULE_USE_DET STD_ON

/*--------------------------------------------- Basic UART Configuration ---------------------------------------------------*/
#define UARTIF_CNT_OF_USED_MODULES 2

#define UARTIF_CNT_OF_ALL_TX_FRAMES  12
#define UARTIF_CNT_OF_ALL_RX_FRAMES  14

#define UARTIF_MODULE1_TX_CNT_FRAMES  5
#define UARTIF_MODULE1_TX_FRAMES_DATA_SIZE  10
#define UARTIF_MODULE1_RX_CNT_FRAMES  6
#define UARTIF_MODULE1_RX_FRAMES_DATA_SIZE  11

#define UARTIF_MODULE2_TX_CNT_FRAMES  7
#define UARTIF_MODULE2_TX_FRAMES_DATA_SIZE  12
#define UARTIF_MODULE2_RX_CNT_FRAMES  8
#define UARTIF_MODULE2_RX_FRAMES_DATA_SIZE  13
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern uint8 uartIfFrameDataTxModule1[UARTIF_MODULE1_TX_CNT_FRAMES][UARTIF_MODULE1_TX_FRAMES_DATA_SIZE + 1];
extern uint8 uartIfFrameDataRxModule1[UARTIF_MODULE1_RX_CNT_FRAMES][UARTIF_MODULE1_RX_FRAMES_DATA_SIZE + 1];
extern uint8 uartIfFrameDataTxModule2[UARTIF_MODULE2_TX_CNT_FRAMES][UARTIF_MODULE2_TX_FRAMES_DATA_SIZE + 1];
extern uint8 uartIfFrameDataRxModule2[UARTIF_MODULE2_RX_CNT_FRAMES][UARTIF_MODULE2_RX_FRAMES_DATA_SIZE + 1];
extern UartIf_ModuleConfigType uartIfModuleConfiguration[UARTIF_CNT_OF_USED_MODULES];
extern UartIf_Buffer_Frame uartIfFrameTxBuffer[UARTIF_CNT_OF_ALL_TX_FRAMES];
extern UartIf_Buffer_Frame uartIfFrameRxBuffer[UARTIF_CNT_OF_ALL_RX_FRAMES];
extern UartIf_Buffer_Ctrl uartIfTxBufferCtrl[UARTIF_CNT_OF_USED_MODULES];
extern UartIf_Buffer_Ctrl uartIfRxBufferCtrl[UARTIF_CNT_OF_USED_MODULES];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/


#endif /* __UartIf_PBCfg_H*/
