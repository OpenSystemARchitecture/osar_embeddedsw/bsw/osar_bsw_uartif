/*****************************************************************************************************************************
* @file        helper.c                                                                                                      *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of functionalities from the "helper" module.                                                   *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "helper.h"
#include "stubs.h"
#include <vcruntime.h>
#include "cmocka.h"
#include "UartIf_Types.h"
#include "UartIf_PBCfg.h"
#include "UartIf_Buffer.h"
#include <stdlib.h>
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*
 * @brief   Helper functiont to set an tx buffer module data >> An check the data also in buffer !!! 
 */
void pushOneElementToTxBuffer(uint8 moduleId, uint8 frameId, boolean readBack)
{
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame *tempBufferFrame;

  if (moduleId == 0)
  {
    retVal = UartIf_Buffer_GetTxWriteFramePointer(moduleId, &tempBufferFrame);
    /* Check return value >> Has to be set by tester */
    assert_int_equal(retVal, mock());
    if ((UARTIF_E_BUFFER_FULL == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataTxModule1[frameId][0]);
    }

    if (TRUE == readBack)
    {
      retVal = UartIf_Buffer_GetTxReadFramePointer(moduleId, &tempBufferFrame);
      /* Check return value >> Has to be set by tester */
      assert_int_equal(retVal, mock());

      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataTxModule1[frameId][0]);
    }
  }
  else
  {
    retVal = UartIf_Buffer_GetTxWriteFramePointer(moduleId, &tempBufferFrame);
    /* Check return value >> Has to be set by tester */
    assert_int_equal(retVal, mock());
    if ((UARTIF_E_BUFFER_FULL == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataTxModule2[frameId][0]);
    }

    if (TRUE == readBack)
    {
      retVal = UartIf_Buffer_GetTxReadFramePointer(moduleId, &tempBufferFrame);
      /* Check return value >> Has to be set by tester */
      assert_int_equal(retVal, mock());

      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataTxModule2[frameId][0]);
    }
  }
}

/*
* @brief   Helper functiont to set an rx buffer module data >> An check the data also in buffer !!!
*/
void pushOneElementToRxBuffer(uint8 moduleId, uint8 frameId, boolean readBack)
{
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame *tempBufferFrame;

  if (moduleId == 0)
  {
    retVal = UartIf_Buffer_GetRxWriteFramePointer(moduleId, &tempBufferFrame);
    /* Check return value >> Has to be set by tester */
    assert_int_equal(retVal, mock());
    if ((UARTIF_E_BUFFER_FULL == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataRxModule1[frameId][0]);
    }

    if (TRUE == readBack)
    {
      retVal = UartIf_Buffer_GetRxReadFramePointer(moduleId, &tempBufferFrame);
      /* Check return value >> Has to be set by tester */
      assert_int_equal(retVal, mock());

      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataRxModule1[frameId][0]);
    }
  }
  else
  {
    retVal = UartIf_Buffer_GetRxWriteFramePointer(moduleId, &tempBufferFrame);
    /* Check return value >> Has to be set by tester */
    assert_int_equal(retVal, mock());
    if ((UARTIF_E_BUFFER_FULL == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataRxModule2[frameId][0]);
    }

    if (TRUE == readBack)
    {
      retVal = UartIf_Buffer_GetRxReadFramePointer(moduleId, &tempBufferFrame);
      /* Check return value >> Has to be set by tester */
      assert_int_equal(retVal, mock());

      /* Check buffer start address */
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataRxModule2[frameId][0]);
    }
  }
}

/*
* @brief   Helper functiont to read an tx buffer module data >> An check the data also in buffer !!!
*/
void popOneElementFromTxBuffer(uint8 moduleId, uint8 frameId, boolean writeElement)
{
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame *tempBufferFrame;

  /* Check if Element has to be written first */
  if (TRUE == writeElement)
  {
    retVal = UartIf_Buffer_GetTxWriteFramePointer(moduleId, &tempBufferFrame);
    assert_int_equal(retVal, mock());
  }

  /* Read Element */
  if (moduleId == 0)
  {
    retVal = UartIf_Buffer_GetTxReadFramePointer(moduleId, &tempBufferFrame);
    assert_int_equal(retVal, mock());

    if ((UARTIF_E_BUFFER_EMPTY == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataTxModule1[frameId][0]);
    }
  }
  else
  {
    retVal = UartIf_Buffer_GetTxReadFramePointer(moduleId, &tempBufferFrame);
    assert_int_equal(retVal, mock());

    if ((UARTIF_E_BUFFER_EMPTY == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataTxModule2[frameId][0]);
    }
  }
}

/*
* @brief   Helper functiont to read an Rx buffer module data >> An check the data also in buffer !!!
*/
void popOneElementFromRxBuffer(uint8 moduleId, uint8 frameId, boolean writeElement)
{
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame *tempBufferFrame;

  /* Check if Element has to be written first */
  if (TRUE == writeElement)
  {
    retVal = UartIf_Buffer_GetRxWriteFramePointer(moduleId, &tempBufferFrame);
    assert_int_equal(retVal, mock());
  }

  /* Read Element */
  if (moduleId == 0)
  {
    retVal = UartIf_Buffer_GetRxReadFramePointer(moduleId, &tempBufferFrame);
    assert_int_equal(retVal, mock());

    if ((UARTIF_E_BUFFER_EMPTY == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataRxModule1[frameId][0]);
    }
  }
  else
  {
    retVal = UartIf_Buffer_GetRxReadFramePointer(moduleId, &tempBufferFrame);
    assert_int_equal(retVal, mock());

    if ((UARTIF_E_BUFFER_EMPTY == retVal) || (UARTIF_E_PENDING == retVal))
    {
      assert_int_equal(tempBufferFrame, NULL_PTR);
      return;
    }
    else
    {
      assert_int_equal(tempBufferFrame->pToActualFrameData, &uartIfFrameDataRxModule2[frameId][0]);
    }
  }
}


/*
* @brief   Helper function to reset all buffer
*/
void resetBuffer(void)
{
  uint8 idx;

  /* Reset Data buffers */
  for (idx = 0; idx <= UARTIF_MODULE1_TX_FRAMES_DATA_SIZE; idx++)
  {
    uartIfFrameDataTxModule1[0][idx] = 0;
    uartIfFrameDataTxModule1[1][idx] = 0;
    uartIfFrameDataTxModule1[2][idx] = 0;
    uartIfFrameDataTxModule1[3][idx] = 0;
    uartIfFrameDataTxModule1[4][idx] = 0;
  }
  for (idx = 0; idx <= UARTIF_MODULE2_TX_FRAMES_DATA_SIZE; idx++)
  {
    uartIfFrameDataTxModule2[0][idx] = 0;
    uartIfFrameDataTxModule2[1][idx] = 0;
    uartIfFrameDataTxModule2[2][idx] = 0;
    uartIfFrameDataTxModule2[3][idx] = 0;
    uartIfFrameDataTxModule2[4][idx] = 0;
    uartIfFrameDataTxModule2[5][idx] = 0;
    uartIfFrameDataTxModule2[6][idx] = 0;
  }
  for (idx = 0; idx <= UARTIF_MODULE1_RX_FRAMES_DATA_SIZE; idx++)
  {
    uartIfFrameDataRxModule1[0][idx] = 0;
    uartIfFrameDataRxModule1[1][idx] = 0;
    uartIfFrameDataRxModule1[2][idx] = 0;
    uartIfFrameDataRxModule1[3][idx] = 0;
    uartIfFrameDataRxModule1[4][idx] = 0;
    uartIfFrameDataRxModule1[5][idx] = 0;
  }
  for (idx = 0; idx <= UARTIF_MODULE2_RX_FRAMES_DATA_SIZE; idx++)
  {
    uartIfFrameDataRxModule2[0][idx] = 0;
    uartIfFrameDataRxModule2[1][idx] = 0;
    uartIfFrameDataRxModule2[2][idx] = 0;
    uartIfFrameDataRxModule2[3][idx] = 0;
    uartIfFrameDataRxModule2[4][idx] = 0;
    uartIfFrameDataRxModule2[5][idx] = 0;
    uartIfFrameDataRxModule2[6][idx] = 0;
    uartIfFrameDataRxModule2[7][idx] = 0;
  }

  /* Reset Cntrl buffers */
  for (idx = 0; idx < UARTIF_CNT_OF_USED_MODULES; idx++)
  {
    uartIfTxBufferCtrl[idx].readElementPos = 0;
    uartIfTxBufferCtrl[idx].writeElementPos = 0;
    uartIfRxBufferCtrl[idx].readElementPos = 0;
    uartIfRxBufferCtrl[idx].writeElementPos = 0;
  }

  /* Reset Frame buffers */
  for (idx = 0; idx < UARTIF_CNT_OF_ALL_TX_FRAMES; idx++)
  {
    uartIfFrameTxBuffer[idx].frameStatus = UART_FRAME_INVALID;
  }
  for (idx = 0; idx < UARTIF_CNT_OF_ALL_RX_FRAMES; idx++)
  {
    uartIfFrameRxBuffer[idx].frameStatus = UART_FRAME_INVALID;
  }
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
