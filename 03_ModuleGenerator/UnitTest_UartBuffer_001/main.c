/*****************************************************************************************************************************
* @file        main.cpp                                                                                                     *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of unit test functionalities from the "Dummy" module.                                         *
*****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <vcruntime.h>
#include <cmocka.h>
#include <stdio.h>
#include "stubs.h"
#include "helper.h"

#include "UartIf_Buffer.c"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Unit tests of Group Generic_Interfaces */
static void UnitTest_UartBuffer_GenericInterface_API_InitMemory(void **state);
static void UnitTest_UartBuffer_GenericInterface_API_GetTxFrameDataSize(void **state);
static void UnitTest_UartBuffer_GenericInterface_API_GetRxFrameDataSize(void **state);
static void UnitTest_UartBuffer_GenericInterface_API_GetActualRxTxLoad(void **state);
static void UnitTest_UartBuffer_GenericInterface_API_GetTxReadWriteFramePointer(void **state);
static void UnitTest_UartBuffer_GenericInterface_API_GetRxReadWriteFramePointer(void **state);

/* Unit tests of Group UartBuffer_BufferAccess */
static void UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_01(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_02(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_03(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_01(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_02(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_03(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetTxReadPointer_04(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetTxReadPointer_05(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetRxReadPointer_06(void **state);
static void UnitTest_UartBuffer_BufferAccess_API_GetRxReadPointer_07(void **state);
static void UnitTest_UartBuffer_BufferAccess_BufferStressTest_08(void **state);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Creating Unit Test Group UartBuffer_GenericInterfaces*/
const struct CMUnitTest UartBuffer_GenericInterfaces[] = {
  cmocka_unit_test(UnitTest_UartBuffer_GenericInterface_API_InitMemory),
  cmocka_unit_test(UnitTest_UartBuffer_GenericInterface_API_GetTxFrameDataSize),
  cmocka_unit_test(UnitTest_UartBuffer_GenericInterface_API_GetRxFrameDataSize),
  cmocka_unit_test(UnitTest_UartBuffer_GenericInterface_API_GetActualRxTxLoad),
  cmocka_unit_test(UnitTest_UartBuffer_GenericInterface_API_GetTxReadWriteFramePointer),
  cmocka_unit_test(UnitTest_UartBuffer_GenericInterface_API_GetRxReadWriteFramePointer),
};

/* Creating Unit Test Group UartBuffer_BufferAccess*/
const struct CMUnitTest UartBuffer_BufferAccess[] = {
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_01),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_02),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_03),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_01),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_02),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_03),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetTxReadPointer_04),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetTxReadPointer_05),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetRxReadPointer_06),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_API_GetRxReadPointer_07),
  cmocka_unit_test(UnitTest_UartBuffer_BufferAccess_BufferStressTest_08),
};
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------- Unit Tests of group UartBuffer_GenericInterfaces -------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*
 * @brief   Unit test to check the API functionality of UartIf_Buffer_InitMemory()
 */
static void UnitTest_UartBuffer_GenericInterface_API_InitMemory(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check of the buffer status is valid\r\n");
  uartIfBufferStatus = UARTIF_E_NOT_OK;
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  retVal = UartIf_Buffer_GetGeneralBufferModuleStatus();
  assert_int_equal(retVal, UARTIF_E_NOT_OK);
  UartIf_Buffer_InitMemory();
  assert_int_equal(uartIfBufferStatus, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetGeneralBufferModuleStatus();
  assert_int_equal(retVal, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  
  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check for all modules the tx frame data length. Configured modules 2.
*          Check valid and invalid values >> Report an Det Error;
*/
static void UnitTest_UartBuffer_GenericInterface_API_GetTxFrameDataSize(void **state)
{
  UartIf_ReturnType retVal;
  uint8 dataSize;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check for all modules the tx frame length. Configured modules 2.\r\n");
  UartIf_Buffer_InitMemory();

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Check first module */
  retVal = UartIf_Buffer_GetTxFrameDataSize(0, &dataSize);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(dataSize, UARTIF_MODULE1_TX_FRAMES_DATA_SIZE);

  /* Check second module */
  retVal = UartIf_Buffer_GetTxFrameDataSize(1, &dataSize);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(dataSize, UARTIF_MODULE2_TX_FRAMES_DATA_SIZE);

  /* Check Fault cases */
  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetTxFrameDataSize(2, &dataSize);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetTxFrameDataSize(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check for all modules the rx frame data length. Configured modules 2.
*          Check valid and invalid values >> Report an Det Error;
*/
static void UnitTest_UartBuffer_GenericInterface_API_GetRxFrameDataSize(void **state)
{
  UartIf_ReturnType retVal;
  uint8 dataSize;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check for all modules the rx frame length. Configured modules 2.\r\n");
  UartIf_Buffer_InitMemory();

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Check first module */
  retVal = UartIf_Buffer_GetRxFrameDataSize(0, &dataSize);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(dataSize, UARTIF_MODULE1_RX_FRAMES_DATA_SIZE);

  /* Check second module */
  retVal = UartIf_Buffer_GetRxFrameDataSize(1, &dataSize);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(dataSize, UARTIF_MODULE2_RX_FRAMES_DATA_SIZE);

  /* Check Fault cases */
  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetRxFrameDataSize(2, &dataSize);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetRxFrameDataSize(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check for the rx and tx buffer load.
*          Check invalid values >> Report an Det Error.
*/
static void UnitTest_UartBuffer_GenericInterface_API_GetActualRxTxLoad(void **state)
{
  UartIf_ReturnType retVal;
  uint8 dataSize;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check for the rx and tx buffer load.\r\n");
  printf("[          ] Check invalid values >> Report an Det Error.\r\n");

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Check Fault cases */
  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(2, &dataSize);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(2, &dataSize);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}


/*
* @brief   This unit test is used to check for the tx read frame pointer API.
*          Check invalid values >> Report an Det Error.
*/
static void UnitTest_UartBuffer_GenericInterface_API_GetTxReadWriteFramePointer(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame data;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check for the tx read frame pointer API.\r\n");
  printf("[          ] Check invalid values >> Report an Det Error.\r\n");

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Check Fault cases */
  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetTxReadFramePointer(2, &data);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetTxReadFramePointer(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetTxWriteFramePointer(2, &data);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check for the rx read frame pointer API.
*          Check invalid values >> Report an Det Error.
*/
static void UnitTest_UartBuffer_GenericInterface_API_GetRxReadWriteFramePointer(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame data;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check for the rx read frame pointer API.\r\n");
  printf("[          ] Check invalid values >> Report an Det Error.\r\n");

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_INDEX_OUT_OF_RANGE);
  will_return(Det_ReportError, E_OK);

  expect_function_call(Det_ReportError);
  will_return(Det_ReportError, UARTIF_DET_MODULE_ID);
  will_return(Det_ReportError, UARTIF_E_NULL_POINTER);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Check Fault cases */
  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetRxReadFramePointer(2, &data);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetRxReadFramePointer(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Invalid module index */
  retVal = UartIf_Buffer_GetRxWriteFramePointer(2, &data);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /* >> Null Pointer */
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, NULL_PTR);
  assert_int_equal(retVal, UARTIF_E_NOT_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}
/*--------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------- Unit Tests of group UartBuffer_BufferAccess ---------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().
*          >> Test with one element on each module without read back. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_01(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().\r\n");
  printf("[          ] >> Test with one element on each module without read back. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToTxBuffer(0, 0, FALSE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToTxBuffer(1, 0, FALSE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().
*          >> Test with one element on each module with read back. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_02(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().\r\n");
  printf("[          ] >> Test with one element on each module with read back. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToTxBuffer(0, 0, TRUE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToTxBuffer(1, 0, TRUE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().
*          >> Test with multiple elements on each module without read back till buffer is full. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetTxWritePointer_03(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().\r\n");
  printf("[          ] >> Test with multiple elements on each module without read back till buffer is full. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  /* Module 1*/
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_BUFFER_FULL);

  /* Module 2*/
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToTxBuffer, UARTIF_E_BUFFER_FULL);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* +++++++++++++++++ Set data for first module +++++++++++++++++++++ */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToTxBuffer(0, 0, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  pushOneElementToTxBuffer(0, 1, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 2);

  pushOneElementToTxBuffer(0, 2, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 3);

  pushOneElementToTxBuffer(0, 3, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 4);

  pushOneElementToTxBuffer(0, 4, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 4);

  /* +++++++++++++++++ Set data for second module +++++++++++++++++++++ */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToTxBuffer(1, 0, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  pushOneElementToTxBuffer(1, 1, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 2);

  pushOneElementToTxBuffer(1, 2, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 3);


  pushOneElementToTxBuffer(1, 3, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 4);

  pushOneElementToTxBuffer(1, 4, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 5);

  pushOneElementToTxBuffer(1, 5, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 6);


  pushOneElementToTxBuffer(1, 6, FALSE);
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 6);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().
*          >> Test with one element on each module without read back. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_01(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetRxWritePointer().\r\n");
  printf("[          ] >> Test with one element on each module without read back. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToRxBuffer(0, 0, FALSE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToRxBuffer(1, 0, FALSE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetRxWritePointer().
*          >> Test with one element on each module with read back. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_02(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetRxWritePointer().\r\n");
  printf("[          ] >> Test with one element on each module with read back. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToRxBuffer(0, 0, TRUE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToRxBuffer(1, 0, TRUE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetRxWritePointer().
*          >> Test with multiple elements on each module without read back till buffer is full. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetRxWritePointer_03(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetRxWritePointer().\r\n");
  printf("[          ] >> Test with multiple elements on each module without read back till buffer is full. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  /* Module 1*/
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_BUFFER_FULL);

  /* Module 2*/
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
  will_return(pushOneElementToRxBuffer, UARTIF_E_BUFFER_FULL);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* +++++++++++++++++ Set data for first module +++++++++++++++++++++ */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToRxBuffer(0, 0, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  pushOneElementToRxBuffer(0, 1, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 2);

  pushOneElementToRxBuffer(0, 2, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 3);

  pushOneElementToRxBuffer(0, 3, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 4);

  pushOneElementToRxBuffer(0, 4, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 5);

  pushOneElementToRxBuffer(0, 5, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 5);

  /* +++++++++++++++++ Set data for second module +++++++++++++++++++++ */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  pushOneElementToRxBuffer(1, 0, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  pushOneElementToRxBuffer(1, 1, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 2);

  pushOneElementToRxBuffer(1, 2, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 3);


  pushOneElementToRxBuffer(1, 3, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 4);

  pushOneElementToRxBuffer(1, 4, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 5);

  pushOneElementToRxBuffer(1, 5, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 6);

  pushOneElementToRxBuffer(1, 6, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 7);


  pushOneElementToRxBuffer(1, 7, FALSE);
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 7);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetTxReadPointer().
*          >> Test read with one element on each module with creation first. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetTxReadPointer_04(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetTxReadPointer().\r\n");
  printf("[          ] >> Test read with one element on each module with creation first. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
 
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  popOneElementFromTxBuffer(0, 0, TRUE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  UartIf_Buffer_IncrementTxReadFramePointer(0);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);


  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  popOneElementFromTxBuffer(1, 0, TRUE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_OK);
  assert_int_equal(bufferLoad, 1);

  UartIf_Buffer_IncrementTxReadFramePointer(1);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetTxWritePointer().
*          >> Test with multiple elements on each moduletill buffer is empty. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetTxReadPointer_05(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame* tempBufferFrame;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetTxReadPointer().\r\n");
  printf("[          ] >> Test with multiple elements on each moduletill buffer is empty. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_BUFFER_EMPTY);


  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromTxBuffer, UARTIF_E_BUFFER_EMPTY);

  /* Prefill Tx Buffer module 1*/
  retVal = UartIf_Buffer_GetTxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);

  /* Prefill Tx Buffer module 2*/
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetTxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 4);

  popOneElementFromTxBuffer(0, 0, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(0);
  popOneElementFromTxBuffer(0, 1, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(0);
  popOneElementFromTxBuffer(0, 2, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(0);
  popOneElementFromTxBuffer(0, 3, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(0);
  popOneElementFromTxBuffer(0, 4, FALSE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 6);

  popOneElementFromTxBuffer(1, 0, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(1);
  popOneElementFromTxBuffer(1, 1, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(1);
  popOneElementFromTxBuffer(1, 2, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(1);
  popOneElementFromTxBuffer(1, 3, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(1);
  popOneElementFromTxBuffer(1, 4, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(1);
  popOneElementFromTxBuffer(1, 5, FALSE);
  UartIf_Buffer_IncrementTxReadFramePointer(1);
  popOneElementFromTxBuffer(1, 6, FALSE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetRxReadPointer().
*          >> Test read with one element on each module with creation first. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetRxReadPointer_06(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetRxReadPointer().\r\n");
  printf("[          ] >> Test read with one element on each module with creation first. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  popOneElementFromRxBuffer(0, 0, TRUE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  popOneElementFromRxBuffer(1, 0, TRUE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used to check the API functionality of UartIf_Buffer_GetRxWritePointer().
*          >> Test with multiple elements on each moduletill buffer is empty. Check also Data buffer load.
*/
static void UnitTest_UartBuffer_BufferAccess_API_GetRxReadPointer_07(void **state)
{
  uint8 bufferLoad;
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame* tempBufferFrame;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used to check the API functionality of UartIf_Buffer_GetRxReadPointer().\r\n");
  printf("[          ] >> Test with multiple elements on each moduletill buffer is empty. Check also Data buffer load.\r\n");

  /* Prepare data for sub test function */
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_BUFFER_EMPTY);


  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_OK);
  will_return(popOneElementFromRxBuffer, UARTIF_E_BUFFER_EMPTY);

  /* Prefill Tx Buffer module 1*/
  retVal = UartIf_Buffer_GetRxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(0, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);

  /* Prefill Tx Buffer module 2*/
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_OK);
  retVal = UartIf_Buffer_GetRxWriteFramePointer(1, &tempBufferFrame);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Set data for first module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(0, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 5);

  popOneElementFromRxBuffer(0, 0, FALSE);
  popOneElementFromRxBuffer(0, 1, FALSE);
  popOneElementFromRxBuffer(0, 2, FALSE);
  popOneElementFromRxBuffer(0, 3, FALSE);
  popOneElementFromRxBuffer(0, 4, FALSE);
  popOneElementFromRxBuffer(0, 5, FALSE);

  retVal = UartIf_Buffer_GetActualTxBufferLoad(0, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);

  /* Set data for second module */
  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);
  assert_int_equal(bufferLoad, 7);

  popOneElementFromRxBuffer(1, 0, FALSE);
  popOneElementFromRxBuffer(1, 1, FALSE);
  popOneElementFromRxBuffer(1, 2, FALSE);
  popOneElementFromRxBuffer(1, 3, FALSE);
  popOneElementFromRxBuffer(1, 4, FALSE);
  popOneElementFromRxBuffer(1, 5, FALSE);
  popOneElementFromRxBuffer(1, 6, FALSE);
  popOneElementFromRxBuffer(1, 7, FALSE);

  retVal = UartIf_Buffer_GetActualRxBufferLoad(1, &bufferLoad);

  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);
  assert_int_equal(bufferLoad, 0);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}

/*
* @brief   This unit test is used check the buffer handling using an stress test of writing and reading the buffer.
*/
static void UnitTest_UartBuffer_BufferAccess_BufferStressTest_08(void **state)
{
  uint8 bufferLoad;
  uint8 idx;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] This unit test is used check the buffer handling using an stress test of writing and reading the buffer.\r\n");

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  for (uint8 idx = 0; idx < 200; idx++)
  {
    will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
    will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
    pushOneElementToTxBuffer(0, (idx % 5), TRUE);
    UartIf_Buffer_IncrementTxReadFramePointer(0);

    will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
    will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
    pushOneElementToRxBuffer(0, (idx % 6), TRUE);

    will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
    will_return(pushOneElementToTxBuffer, UARTIF_E_OK);
    pushOneElementToTxBuffer(1, (idx % 7), TRUE);
    UartIf_Buffer_IncrementTxReadFramePointer(1);

    will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
    will_return(pushOneElementToRxBuffer, UARTIF_E_OK);
    pushOneElementToRxBuffer(1, (idx % 8), TRUE);
  }

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  resetBuffer();

  (void)state; /* unused */
}




/*--------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ Unit Tests of group ... -------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define CNT_OF_TEST_GROUPS 2
int main()
{
  int result[CNT_OF_TEST_GROUPS], idx = 0;
  /* Setup Console for Test Output */
  printf("Startup of Module Tests for Module xyz. \r\nTest Framework: CMocka 1.1.1 \r\n");

  /*#################################### Run CMocka group tests #############################################*/
  printf("Startup unit test group \"UartBuffer_GenericInterfaces\"\r\n");
  result[0] = cmocka_run_group_tests(UartBuffer_GenericInterfaces, NULL, NULL);
  result[1] = cmocka_run_group_tests(UartBuffer_BufferAccess, NULL, NULL);

  
  

  /* Print result */
  printf("\r\n\r\n=====================================================================================\r\n");
  printf("Test summary:\r\n\r\n");
  for (idx = 0; idx < CNT_OF_TEST_GROUPS; idx++)
  {
    printf("Testgroup %d >> Cnt of errors: %d\r\n", idx, result[idx]);
  }
  printf("=====================================================================================\r\n\r\n");

  /* wait for user key to shutdown system */
  printf("Pres any key to exit test environment \r\n");
  getch();
    return 0;
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

