/*****************************************************************************************************************************
* @file        Stubs.c                                                                                                      *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of functionalities from the "Stubs" module.                                                   *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "stubs.h"
#include <vcruntime.h>
#include "cmocka.h"
#include "UartIf_Types.h"
#include "UartIf_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
uint8 uartIfFrameDataTxModule1[UARTIF_MODULE1_TX_CNT_FRAMES][UARTIF_MODULE1_TX_FRAMES_DATA_SIZE + 1];
uint8 uartIfFrameDataRxModule1[UARTIF_MODULE1_RX_CNT_FRAMES][UARTIF_MODULE1_RX_FRAMES_DATA_SIZE + 1];
uint8 uartIfFrameDataTxModule2[UARTIF_MODULE2_TX_CNT_FRAMES][UARTIF_MODULE2_TX_FRAMES_DATA_SIZE + 1];
uint8 uartIfFrameDataRxModule2[UARTIF_MODULE2_RX_CNT_FRAMES][UARTIF_MODULE2_RX_FRAMES_DATA_SIZE + 1];

UartIf_ModuleConfigType uartIfModuleConfiguration[UARTIF_CNT_OF_USED_MODULES] =
{
  {NULL_PTR, NULL_PTR, NULL_PTR, NULL_PTR },
  {NULL_PTR, NULL_PTR, NULL_PTR, NULL_PTR }
};

UartIf_Buffer_Frame uartIfFrameTxBuffer[UARTIF_CNT_OF_ALL_TX_FRAMES] =
{
  { 0, &uartIfFrameDataTxModule1[0][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[1][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[2][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[3][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[4][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[0][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[1][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[2][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[3][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[4][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[5][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule2[6][0], UART_FRAME_INVALID }
};

UartIf_Buffer_Frame uartIfFrameRxBuffer[UARTIF_CNT_OF_ALL_RX_FRAMES] =
{
  { 0, &uartIfFrameDataRxModule1[0][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[1][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[2][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[3][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[4][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[5][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[0][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[1][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[2][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[3][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[4][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[5][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[6][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule2[7][0], UART_FRAME_INVALID },
};

UartIf_Buffer_Ctrl uartIfTxBufferCtrl[UARTIF_CNT_OF_USED_MODULES] =
{
  { UARTIF_MODULE1_TX_CNT_FRAMES, 0, 0, UARTIF_MODULE1_TX_FRAMES_DATA_SIZE, 0 },
  { UARTIF_MODULE2_TX_CNT_FRAMES, 0, 0, UARTIF_MODULE2_TX_FRAMES_DATA_SIZE, 5 }
};

UartIf_Buffer_Ctrl uartIfRxBufferCtrl[UARTIF_CNT_OF_USED_MODULES] =
{
  { UARTIF_MODULE1_RX_CNT_FRAMES, 0, 0, UARTIF_MODULE1_RX_FRAMES_DATA_SIZE , 0 },
  { UARTIF_MODULE2_RX_CNT_FRAMES, 0, 0, UARTIF_MODULE2_RX_FRAMES_DATA_SIZE , 6 }
};


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
Std_ReturnType Det_ReportError(uint8 moduleId, uint8 errorId)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_int_equal(errorId, mock());
  return (Std_ReturnType)mock();
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/

