/*****************************************************************************************************************************
* @file        STUB >> Uart_Buffer.h                                                                                        *
* @author      OSAR Team S.Reinemuth                                                                                        *
* @date        18.02.2018 11:52:01                                                                                          *
* @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
*              informations of the "Uart_Buffer" module.                                                                    *
*                                                                                                                           *
* @details     The uart buffer module implements the TX / RX Ring buffer handling for the higher layer modules also for the *
*              Mcal_Uart module. The buffer itself would be generated using the UartGenerator. The frame data length also   *
*              as th count of frames is adaptable.                                                                          *
*                                                                                                                           *
* @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
*              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
*              License, or (at your option) any later version.                                                              *
*                                                                                                                           *
*              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
*              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
*              License for more details http://www.gnu.org/licenses/.                                                       *
* @version     0.1.0                                                                                                        *
*                                                                                                                           *
* @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UART_BUFFER_H
#define __UART_BUFFER_H

/**
* @addtogroup Uart
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "UartIf_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern UartIf_ReturnType uartIfBufferStatus;

/**
* @brief           Request the actual buffer module status
* @param[]         None
* @retval          UartIf_ReturnType >> Equal to the current buffer status
*                  UARTIF_E_OK
*                  UARTIF_E_BUFFER_OVERFLOW
*/
#define UartIf_Buffer_GetGeneralBufferModuleStatus()  (UartIf_ReturnType)uartIfBufferStatus
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
* @brief           Init the uart buffer memory with an valid configuration
* @param[]         None
* @retval          None
* @details         This function shall be called during system startup. And before calling the init and mainfuntion.
*/
void UartIf_Buffer_InitMemory(void);

/**
* @brief           Request the actual RX Buffer load from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8* pActualLoad  >> Output variable
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetActualRxBufferLoad(uint8 moduleId, uint8* pActualLoad);

/**
* @brief           Request the actual TX Buffer load from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8* pActualLoad  >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_BUFFER_EMPTY
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetActualTxBufferLoad(uint8 moduleId, uint8* pActualLoad);

/**
* @brief           Request the actual RX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetRxReadFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame);

/**
* @brief           Request the actual TX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame *pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_EMPTY
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_BUFFER_OVERFLOW
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetTxReadFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame);

/**
* @brief           Request the actual RX Buffer Frame Write Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetRxWriteFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame);

/**
* @brief           Request the actual TX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetTxWriteFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame);

/**
* @brief           Request the data frame size of the tx buffer from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8 *pdataSize
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetRxFrameDataSize(uint8 moduleId, uint8 *pDataSize);

/**
* @brief           Request the data frame size of the tx buffer from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8 *pdataSize
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetTxFrameDataSize(uint8 moduleId, uint8 *pDataSize);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/

#endif /* __UART_BUFFER_H*/
