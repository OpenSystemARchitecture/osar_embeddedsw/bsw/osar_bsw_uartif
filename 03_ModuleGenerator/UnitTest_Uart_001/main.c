/*****************************************************************************************************************************
* @file        main.cpp                                                                                                     *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of unit test functionalities from the "Dummy" module.                                         *
*****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <vcruntime.h>
#include <cmocka.h>
#include <stdio.h>
#include "stubs.h"
#include "helper.h"


#include "UartIf.c"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Unit tests of Group Generic_Interfaces */
static void UnitTest_Uart_GenericInterface_API_InitMemory(void **state);
static void UnitTest_Uart_GenericInterface_API_Init(void **state);
static void UnitTest_Uart_GenericInterface_API_GetReceiveData01(void **state);
static void UnitTest_Uart_GenericInterface_API_GetReceiveData02(void **state);
static void UnitTest_Uart_GenericInterface_API_SetTransmitData01(void **state);
static void UnitTest_Uart_GenericInterface_API_SetTransmitData02(void **state);

/* Unit tests of Group InternalBehaviour */
static void UnitTest_Uart_InternalBehaviour_01(void **state);
static void UnitTest_Uart_InternalBehaviour_02(void **state);
static void UnitTest_Uart_InternalBehaviour_03(void **state);
static void UnitTest_Uart_InternalBehaviour_04(void **state);
static void UnitTest_Uart_InternalBehaviour_05(void **state);
static void UnitTest_Uart_InternalBehaviour_06(void **state);
static void UnitTest_Uart_InternalBehaviour_07(void **state);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Creating Unit Test Group UartBuffer_GenericInterfaces*/
const struct CMUnitTest UartBuffer_GenericInterfaces[] = {
  cmocka_unit_test(UnitTest_Uart_GenericInterface_API_InitMemory),
  cmocka_unit_test(UnitTest_Uart_GenericInterface_API_Init),
  cmocka_unit_test(UnitTest_Uart_GenericInterface_API_GetReceiveData01),
  cmocka_unit_test(UnitTest_Uart_GenericInterface_API_GetReceiveData02),
  cmocka_unit_test(UnitTest_Uart_GenericInterface_API_SetTransmitData01),
  cmocka_unit_test(UnitTest_Uart_GenericInterface_API_SetTransmitData02),
};

/* Creating Unit Test Group UartBuffer_InternalBehaviour*/
const struct CMUnitTest UartBuffer_InternalBehaviour[] = {
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_01),
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_02),
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_03),
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_04),
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_05),
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_06),
  cmocka_unit_test(UnitTest_Uart_InternalBehaviour_07),
};


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------- Unit Tests of group UartBuffer_GenericInterfaces -------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*
 * @brief   Unit test to check the API functionality of UartIf_InitMemory()
 */
static void UnitTest_Uart_GenericInterface_API_InitMemory(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the API functionality of UartIf_InitMemory()\r\n");

  expect_function_call(UartIf_Buffer_InitMemory);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  
  UartIf_InitMemory();
  assert_int_equal(uartIfActualStatemachineState, UART_STATEMACHINESTATE_UNINIT);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  
  (void)state; /* unused */
}

/*
* @brief   Unit test to check the API functionality of Uart_Init()
*/
static void UnitTest_Uart_GenericInterface_API_Init(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the API functionality of Uart_Init\r\n");

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  UartIf_Init();
  assert_int_equal(uartIfActualStatemachineState, UART_STATEMACHINESTATE_RUN);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   Unit test to check the API functionality of Uart_GetReceiveData()
*          Request API when Buffer is empty.
*/
static void UnitTest_Uart_GenericInterface_API_GetReceiveData01(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_RxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the API functionality of Uart_Init\r\n");
  printf("[          ] Request API when Buffer is empty.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = 0;
    frameData2[idx] = 0;
  }

  expect_function_calls(UartIf_Buffer_GetActualRxBufferLoad, 2);

  /* Module 1 */
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, UARTIF_E_BUFFER_EMPTY);
  frame1.moduleID = 0;
  frame1.pData = &frameData1[0];

  /* Module 2 */
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, UARTIF_E_BUFFER_EMPTY);
  frame2.moduleID = 1;
  frame2.pData = &frameData2[0];

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Module 1 */
  retVal = UartIf_GetReceivedData(&frame1);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);

  /* Module 2 */
  retVal = UartIf_GetReceivedData(&frame2);
  assert_int_equal(retVal, UARTIF_E_BUFFER_EMPTY);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   Unit test to check the API functionality of Uart_GetReceiveData()
*          Request API when Buffer has one element.
*/
static void UnitTest_Uart_GenericInterface_API_GetReceiveData02(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_RxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  UartIf_Buffer_Frame refFrame1, refFrame2;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the API functionality of Uart_Init\r\n");
  printf("[          ] Request API when Buffer is empty.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = 0;
    frameData2[idx] = 0;
    refFrameData1[idx] = rand() % 10;
    refFrameData2[idx] = rand() % 10;
  }

  expect_function_call(UartIf_Buffer_GetActualRxBufferLoad);
  expect_function_call(UartIf_Buffer_GetRxFrameDataSize);
  expect_function_call(UartIf_Buffer_GetRxReadFramePointer);

  expect_function_call(UartIf_Buffer_GetActualRxBufferLoad);
  expect_function_call(UartIf_Buffer_GetRxFrameDataSize);
  expect_function_call(UartIf_Buffer_GetRxReadFramePointer);


  /* ========== FIRST CALL ========== */
  /* +++++ Module 1 +++++ */
  frame1.moduleID = 0;
  frame1.pData = &frameData1[0];
  
  refFrame1.frameDataLength = framData1Size;
  refFrame1.frameStatus = UART_FRAME_OK;
  refFrame1.pToActualFrameData = &refFrameData1[0];

  /* API UartIf_Buffer_GetActualRxBufferLoad */
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetRxFrameDataSize, 0);
  will_return(UartIf_Buffer_GetRxFrameDataSize, framData1Size);
  will_return(UartIf_Buffer_GetRxFrameDataSize, UARTIF_E_OK);

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetRxReadFramePointer, 0);
  will_return(UartIf_Buffer_GetRxReadFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetRxReadFramePointer, UARTIF_E_OK);


  /* +++++ Module 2 +++++ */
  frame2.moduleID = 1;
  frame2.pData = &frameData2[0];
  
  refFrame2.frameDataLength = framData2Size;
  refFrame2.frameStatus = UART_FRAME_OK;
  refFrame2.pToActualFrameData = &refFrameData2[0];

  /* API UartIf_Buffer_GetActualRxBufferLoad */
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualRxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetRxFrameDataSize, 1);
  will_return(UartIf_Buffer_GetRxFrameDataSize, framData2Size);
  will_return(UartIf_Buffer_GetRxFrameDataSize, UARTIF_E_OK);

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetRxReadFramePointer, 1);
  will_return(UartIf_Buffer_GetRxReadFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetRxReadFramePointer, UARTIF_E_OK);



  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Module 1 */
  retVal = UartIf_GetReceivedData(&frame1);
  assert_int_equal(retVal, UARTIF_E_OK);

  /* Check Data */
  for (idx = 0; idx < framData1Size; idx++)
  {
    assert_int_equal(frame1.pData[idx], refFrame1.pToActualFrameData[idx]);
  }
  assert_int_equal(frame1.pData[framData1Size], 0);
  assert_int_equal(frame1.frameState, UART_FRAME_OK);
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_INVALID);
  assert_int_equal(frame1.cntOfData, framData1Size);

  /* Module 2 */
  retVal = UartIf_GetReceivedData(&frame2);
  assert_int_equal(retVal, UARTIF_E_OK);

  /* Check Data */
  for (idx = 0; idx < framData2Size; idx++)
  {
    assert_int_equal(frame2.pData[idx], refFrame2.pToActualFrameData[idx]);
  }
  assert_int_equal(frame2.pData[framData2Size], 0);
  assert_int_equal(frame2.frameState, UART_FRAME_OK);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_INVALID);
  assert_int_equal(frame2.cntOfData, framData2Size);


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   Unit test to check the API functionality of UartIf_SetTransmitData()
*          Request API when Buffer is full.
*/
static void UnitTest_Uart_GenericInterface_API_SetTransmitData01(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_RxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the API functionality of UartIf_SetTransmitData()\r\n");
  printf("[          ] Request API when Buffer is full.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = 0;
    frameData2[idx] = 0;
  }

  expect_function_calls(UartIf_Buffer_GetTxFrameDataSize, 2);

  /* Module 1 */
  will_return(UartIf_Buffer_GetTxFrameDataSize, 0);
  will_return(UartIf_Buffer_GetTxFrameDataSize, 10);
  will_return(UartIf_Buffer_GetTxFrameDataSize, UARTIF_E_BUFFER_FULL);
  frame1.moduleID = 0;
  frame1.pData = &frameData1[0];

  /* Module 2 */
  will_return(UartIf_Buffer_GetTxFrameDataSize, 1);
  will_return(UartIf_Buffer_GetTxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetTxFrameDataSize, UARTIF_E_BUFFER_FULL);
  frame2.moduleID = 1;
  frame2.pData = &frameData2[0];

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Module 1 */
  retVal = UartIf_SetTransmitData(&frame1);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);

  /* Module 2 */
  retVal = UartIf_SetTransmitData(&frame2);
  assert_int_equal(retVal, UARTIF_E_BUFFER_FULL);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   Unit test to check the API functionality of UartIf_SetTransmitData()
*          Request API when Buffer has space.
*/
static void UnitTest_Uart_GenericInterface_API_SetTransmitData02(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_TxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  UartIf_Buffer_Frame refFrame1, refFrame2;

  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the API functionality of UartIf_SetTransmitData()\r\n");
  printf("[          ] Request API when Buffer has space.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  expect_function_call(UartIf_Buffer_GetTxFrameDataSize);
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  expect_function_call(UartIf_Buffer_GetTxWriteFramePointer);

  expect_function_call(UartIf_Buffer_GetTxFrameDataSize);
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  expect_function_call(UartIf_Buffer_GetTxWriteFramePointer);


  /* ========== FIRST CALL ========== */
  /* +++++ Module 1 +++++ */
  frame1.moduleID = 0;
  frame1.pData = &frameData1[0];
  frame1.cntOfData = framData1Size;

  refFrame1.frameStatus = UART_FRAME_INVALID;
  refFrame1.pToActualFrameData = &refFrameData1[0];

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetTxFrameDataSize, 0);
  will_return(UartIf_Buffer_GetTxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetTxFrameDataSize, UARTIF_E_OK);

  /* API UartIf_Buffer_GetActualRxBufferLoad */
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetTxWriteFramePointer, 0);
  will_return(UartIf_Buffer_GetTxWriteFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetTxWriteFramePointer, UARTIF_E_OK);


  /* +++++ Module 2 +++++ */
  frame2.moduleID = 1;
  frame2.pData = &frameData2[0];
  frame2.cntOfData = framData2Size;

  refFrame2.frameStatus = UART_FRAME_INVALID;
  refFrame2.pToActualFrameData = &refFrameData2[0];

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetTxFrameDataSize, 1);
  will_return(UartIf_Buffer_GetTxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetTxFrameDataSize, UARTIF_E_OK);

  /* API UartIf_Buffer_GetActualRxBufferLoad */
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetRxFrameDataSize */
  will_return(UartIf_Buffer_GetTxWriteFramePointer, 1);
  will_return(UartIf_Buffer_GetTxWriteFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetTxWriteFramePointer, UARTIF_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  /* Module 1 */
  retVal = UartIf_SetTransmitData(&frame1);
  assert_int_equal(retVal, UARTIF_E_OK);

  /* Check Data */
  for (idx = 0; idx < framData1Size; idx++)
  {
    assert_int_equal(frame1.pData[idx], refFrame1.pToActualFrameData[idx]);
  }
  assert_int_equal(refFrame1.pToActualFrameData[framData1Size], 0);
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_READY);
  assert_int_equal(refFrame1.frameDataLength, framData1Size);

  /* Module 2 */
  retVal = UartIf_SetTransmitData(&frame2);
  assert_int_equal(retVal, UARTIF_E_OK);

  /* Check Data */
  for (idx = 0; idx < framData2Size; idx++)
  {
    assert_int_equal(frame2.pData[idx], refFrame2.pToActualFrameData[idx]);
  }
  assert_int_equal(refFrame2.pToActualFrameData[framData2Size], 0);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_READY);
  assert_int_equal(refFrame2.frameDataLength, framData2Size);


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*--------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------- Unit Tests of group UartBuffer_InternalBehaviour -------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour if TX buffer is empty.
*/
static void UnitTest_Uart_InternalBehaviour_01(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_RxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour if buffer is empty.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = 0;
    frameData2[idx] = 0;
  }

  expect_function_calls(UartIf_Buffer_InitMemory, 1);
  /* Init request after Init memory */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad,2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  UartIf_InitMemory();

  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  UartIf_Mainfunction();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour if tx buffers have data.
*/
static void UnitTest_Uart_InternalBehaviour_02(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_TxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  uint8 framData1Timeout = 100, framData2Timeout = 200;
  UartIf_Buffer_Frame refFrame1, refFrame2;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour if tx buffers have data.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  expect_function_call(UartIf_Buffer_InitMemory);
  UartIf_InitMemory();

  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;

  refFrame1.frameStatus = UART_FRAME_READY;
  refFrame1.frameDataLength = framData1Size;
  refFrame1.pToActualFrameData = &refFrameData1[0];
  refFrame2.frameStatus = UART_FRAME_READY;
  refFrame2.frameDataLength = framData2Size;
  refFrame2.pToActualFrameData = &refFrameData2[0];

  /* >>>>>>>>> Setup Date for Modules <<<<<<<<<< */
  /* +++++ Module 1 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 0);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 0);
  will_return(Uart_Transmit, refFrame1.frameDataLength);
  will_return(Uart_Transmit, E_PENDING);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 0);


  /* +++++ Module 2 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 1);
  will_return(Uart_Transmit, refFrame2.frameDataLength);
  will_return(Uart_Transmit, E_PENDING);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 1);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  UartIf_Mainfunction();
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_PENDING);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_PENDING);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}


/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour if tx buffers have data >> Excatly the TX Notification functionality.
*/
static void UnitTest_Uart_InternalBehaviour_03(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_TxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  uint8 framData1Timeout = 100, framData2Timeout = 200;
  UartIf_Buffer_Frame refFrame1, refFrame2;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour if tx buffers have data >> Excatly the TX Notification functionality.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  expect_function_call(UartIf_Buffer_InitMemory);
  UartIf_InitMemory();

  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;

  refFrame1.frameStatus = UART_FRAME_READY;
  refFrame1.frameDataLength = framData1Size;
  refFrame1.pToActualFrameData = &refFrameData1[0];
  refFrame2.frameStatus = UART_FRAME_READY;
  refFrame2.frameDataLength = framData2Size;
  refFrame2.pToActualFrameData = &refFrameData2[0];

  /* >>>>>>>>> Setup Date for Modules <<<<<<<<<< */
  /* +++++ Module 1 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 0);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 0);
  will_return(Uart_Transmit, refFrame1.frameDataLength);
  will_return(Uart_Transmit, E_PENDING);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 0);


  /* +++++ Module 2 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 1);
  will_return(Uart_Transmit, refFrame2.frameDataLength);
  will_return(Uart_Transmit, E_PENDING);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 1);

  UartIf_Mainfunction();
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_PENDING);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_PENDING);


  /* Notification functions */
  expect_function_call(mod2TxIsrNotFnc);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  UartIf_TxNotification(0);
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_OK);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_PENDING);

  UartIf_TxIsrNotification(1);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/

  (void)state; /* unused */
}

/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour if tx buffers have data and mcal uart module would throw a tx notification during processing module data >> So called synchronous processing.
*          Test with just one data element to be transmitted.
*/
static void UnitTest_Uart_InternalBehaviour_04(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_TxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  uint8 framData1Timeout = 100, framData2Timeout = 200;
  UartIf_Buffer_Frame refFrame1, refFrame2;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour if tx buffers have data and mcal uart module would throw a tx notification during processing module data >> So called synchronous processing. Test with just one data element to be transmitted.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  expect_function_call(UartIf_Buffer_InitMemory);
  UartIf_InitMemory();

  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;

  refFrame1.frameStatus = UART_FRAME_READY;
  refFrame1.frameDataLength = framData1Size;
  refFrame1.pToActualFrameData = &refFrameData1[0];
  refFrame2.frameStatus = UART_FRAME_READY;
  refFrame2.frameDataLength = framData2Size;
  refFrame2.pToActualFrameData = &refFrameData2[0];

  /* >>>>>>>>> Setup Date for Modules <<<<<<<<<< */
  /* +++++ Module 1 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 0);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 0);
  will_return(Uart_Transmit, refFrame1.frameDataLength);
  will_return(Uart_Transmit, E_OK);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 0);

  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  /* Notification functions */
  expect_function_call(mod1TxNotFnc);

  /* +++++ Module 2 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 1);
  will_return(Uart_Transmit, refFrame2.frameDataLength);
  will_return(Uart_Transmit, E_OK);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 1);

  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  throwTxNotificationInMcalUartTransmitFnc = TRUE;

  /* Notification functions */
  expect_function_call(mod2TxNotFnc);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  UartIf_Mainfunction();
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_OK);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  throwTxNotificationInMcalUartTransmitFnc = FALSE;
  (void)state; /* unused */
}

/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour if tx buffers have data and mcal uart module would throw a tx notification during processing module data >> So called synchronous processing.
*          Test with just two data element to be transmitted.
*/
static void UnitTest_Uart_InternalBehaviour_05(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_TxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  uint8 framData1Timeout = 100, framData2Timeout = 200;
  UartIf_Buffer_Frame refFrame1, refFrame2;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour if tx buffers have data and mcal uart module would throw a tx notification during processing module data >> So called synchronous processing. "
    "Test with just two data element to be transmitted.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  expect_function_call(UartIf_Buffer_InitMemory);
  UartIf_InitMemory();

  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;

  refFrame1.frameStatus = UART_FRAME_READY;
  refFrame1.frameDataLength = framData1Size;
  refFrame1.pToActualFrameData = &refFrameData1[0];
  refFrame2.frameStatus = UART_FRAME_READY;
  refFrame2.frameDataLength = framData2Size;
  refFrame2.pToActualFrameData = &refFrameData2[0];

  /* >>>>>>>>> Setup Date for Modules <<<<<<<<<< */
  /* +++++ Module 1 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 0);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 0);
  will_return(Uart_Transmit, refFrame1.frameDataLength);
  will_return(Uart_Transmit, E_OK);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 0);

  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 0);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 0);
  will_return(Uart_Transmit, refFrame1.frameDataLength);
  will_return(Uart_Transmit, E_OK);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 0);

  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  /* Notification functions */
  expect_function_call(mod1TxNotFnc);

  /* +++++ Module 2 +++++ */
  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_FULL);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 1);
  will_return(Uart_Transmit, refFrame2.frameDataLength);
  will_return(Uart_Transmit, E_OK);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 1);

  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_OK);

  /* API UartIf_Buffer_GetTxReadFramePointer */
  expect_function_call(UartIf_Buffer_GetTxReadFramePointer);
  will_return(UartIf_Buffer_GetTxReadFramePointer, 1);
  will_return(UartIf_Buffer_GetTxReadFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetTxReadFramePointer, E_OK);

  /* API Uart_Transmit */
  expect_function_call(Uart_Transmit);
  will_return(Uart_Transmit, 1);
  will_return(Uart_Transmit, refFrame2.frameDataLength);
  will_return(Uart_Transmit, E_OK);

  /* API UartIf_Buffer_IncrementTxReadFramePointer */
  expect_function_call(UartIf_Buffer_IncrementTxReadFramePointer);
  will_return(UartIf_Buffer_IncrementTxReadFramePointer, 1);

  /* API UartIf_Buffer_GetActualTxBufferLoad */
  expect_function_call(UartIf_Buffer_GetActualTxBufferLoad);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  /* Notification functions */
  expect_function_call(mod2TxNotFnc);

  throwTxNotificationInMcalUartTransmitFnc = TRUE;
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  UartIf_Mainfunction();
  assert_int_equal(refFrame1.frameStatus, UART_FRAME_OK);
  assert_int_equal(refFrame2.frameStatus, UART_FRAME_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  throwTxNotificationInMcalUartTransmitFnc = FALSE;
  (void)state; /* unused */
}

/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour if Rx Notifications. >> Buffer empty
*/
static void UnitTest_Uart_InternalBehaviour_06(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_RxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  uint8 framData1Timeout = 100, framData2Timeout = 200;
  UartIf_Buffer_Frame refFrame1, refFrame2;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour if Rx Notifications. >> Buffer empty\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  /* +++++ Module 1 +++++ */
  frame1.cntOfData = framData1Size;
  frame1.frameState = UART_FRAME_OK;
  frame1.moduleID = 0;
  frame1.pData = &refFrameData1[0];

  refFrame1.pToActualFrameData = &refFrameData1[0];
  refFrame1.frameDataLength = 0;
  refFrame1.frameStatus = UART_FRAME_INVALID;

  expect_function_call(UartIf_Buffer_GetRxFrameDataSize);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 0);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetRxFrameDataSize, UARTIF_E_OK);

  expect_function_call(UartIf_Buffer_GetRxWriteFramePointer);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, 0);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, UARTIF_E_OK);

  /* +++++ Module 2 +++++ */
  frame2.cntOfData = framData2Size;
  frame2.frameState = UART_FRAME_OK;
  frame2.moduleID = 1;
  frame2.pData = &refFrameData2[0];

  refFrame2.pToActualFrameData = &refFrameData2[0];
  refFrame2.frameDataLength = 0;
  refFrame2.frameStatus = UART_FRAME_INVALID;

  expect_function_call(UartIf_Buffer_GetRxFrameDataSize);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 1);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetRxFrameDataSize, UARTIF_E_OK);

  expect_function_call(UartIf_Buffer_GetRxWriteFramePointer);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, 1);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, UARTIF_E_OK);

  /* Notification functions */
  expect_function_call(mod2RxIsrNotFnc);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  UartIf_RxNotification(frame1);

  for (idx = 0; idx < framData1Size; idx++)
  {
    assert_int_equal(frame1.pData[idx], refFrame1.pToActualFrameData[idx]);
  }
  assert_int_equal(0, refFrame1.pToActualFrameData[framData1Size]);
  assert_int_equal(frame1.cntOfData, refFrame1.frameDataLength);
  assert_int_equal(frame1.frameState, refFrame1.frameStatus);

  UartIf_RxIsrNotification(frame2);

  for (idx = 0; idx < framData2Size; idx++)
  {
    assert_int_equal(frame2.pData[idx], refFrame2.pToActualFrameData[idx]);
  }
  assert_int_equal(0, refFrame2.pToActualFrameData[framData1Size]);
  assert_int_equal(frame2.cntOfData, refFrame2.frameDataLength);
  assert_int_equal(frame2.frameState, refFrame2.frameStatus);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

/*
* @brief   Unit test to check the internal bahviour of the uart module.
*          Check behaviour of Rx Notification processing.
*/
static void UnitTest_Uart_InternalBehaviour_07(void **state)
{
  UartIf_ReturnType retVal;
  UartIf_RxFrameType frame1, frame2;
  uint8 frameData1[100], frameData2[100], idx;
  uint8 refFrameData1[100], refFrameData2[100];
  uint8 framData1Size = 50, framData2Size = 99;
  uint8 framData1Timeout = 100, framData2Timeout = 200;
  UartIf_Buffer_Frame refFrame1, refFrame2;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] Unit test to check the internal bahviour of the uart module.\r\n");
  printf("[          ] Check behaviour of Rx Notification processing.\r\n");

  /* Pre Data Setup */
  for (idx = 0; idx < 100; idx++)
  {
    frameData1[idx] = rand() % 10;
    frameData2[idx] = rand() % 10;
    refFrameData1[idx] = 0;
    refFrameData2[idx] = 0;
  }

  expect_function_calls(UartIf_Buffer_InitMemory, 1);
  UartIf_InitMemory();

  /* +++++ Module 1 +++++ */
  frame1.cntOfData = framData1Size;
  frame1.frameState = UART_FRAME_OK;
  frame1.moduleID = 0;
  frame1.pData = &refFrameData1[0];

  refFrame1.pToActualFrameData = &refFrameData1[0];
  refFrame1.frameDataLength = 0;
  refFrame1.frameStatus = UART_FRAME_INVALID;

  expect_function_call(UartIf_Buffer_GetRxFrameDataSize);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 0);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetRxFrameDataSize, UARTIF_E_OK);

  expect_function_call(UartIf_Buffer_GetRxWriteFramePointer);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, 0);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, &refFrame1);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, UARTIF_E_OK);

  /* +++++ Module 2 +++++ */
  frame2.cntOfData = framData2Size;
  frame2.frameState = UART_FRAME_OK;
  frame2.moduleID = 1;
  frame2.pData = &refFrameData2[0];

  refFrame2.pToActualFrameData = &refFrameData2[0];
  refFrame2.frameDataLength = 0;
  refFrame2.frameStatus = UART_FRAME_INVALID;

  expect_function_call(UartIf_Buffer_GetRxFrameDataSize);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 1);
  will_return(UartIf_Buffer_GetRxFrameDataSize, 100);
  will_return(UartIf_Buffer_GetRxFrameDataSize, UARTIF_E_OK);

  expect_function_call(UartIf_Buffer_GetRxWriteFramePointer);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, 1);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, &refFrame2);
  will_return(UartIf_Buffer_GetRxWriteFramePointer, UARTIF_E_OK);

  /*==================== Setup Tx Buffer ================================== */
  
  /* Init request after Init memory */
  expect_function_calls(UartIf_Buffer_GetActualTxBufferLoad, 2);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);

  will_return(UartIf_Buffer_GetActualTxBufferLoad, 1);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, 0);
  will_return(UartIf_Buffer_GetActualTxBufferLoad, UARTIF_E_BUFFER_EMPTY);
  
  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;

  /* Notification functions */
  expect_function_call(mod1RxNotFnc);
  expect_function_call(mod2RxNotFnc);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/
  UartIf_RxNotification(frame1);

  for (idx = 0; idx < framData1Size; idx++)
  {
    assert_int_equal(frame1.pData[idx], refFrame1.pToActualFrameData[idx]);
  }
  assert_int_equal(0, refFrame1.pToActualFrameData[framData1Size]);
  assert_int_equal(frame1.cntOfData, refFrame1.frameDataLength);
  assert_int_equal(frame1.frameState, refFrame1.frameStatus);

  UartIf_RxNotification(frame2);

  for (idx = 0; idx < framData2Size; idx++)
  {
    assert_int_equal(frame2.pData[idx], refFrame2.pToActualFrameData[idx]);
  }
  assert_int_equal(0, refFrame2.pToActualFrameData[framData1Size]);
  assert_int_equal(frame2.cntOfData, refFrame2.frameDataLength);
  assert_int_equal(frame2.frameState, refFrame2.frameStatus);

  /* Process mainfunction */
  UartIf_Mainfunction();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define CNT_OF_TEST_GROUPS 2
int main()
{
  int result[CNT_OF_TEST_GROUPS], idx = 0;
  /* Setup Console for Test Output */
  printf("Startup of Module Tests for Module UART. \r\nTest Framework: CMocka 1.1.1 \r\n");

  /*#################################### Run CMocka group tests #############################################*/
  printf("Startup unit test group \"Uart_GenericInterfaces\"\r\n");
  result[0] = cmocka_run_group_tests(UartBuffer_GenericInterfaces, NULL, NULL);
  printf("\r\nStartup unit test group \"Uart_GenericInterfaces\"\r\n");
  result[1] = cmocka_run_group_tests(UartBuffer_InternalBehaviour, NULL, NULL);


  /* Print result */
  printf("\r\n\r\n=====================================================================================\r\n");
  printf("Test summary:\r\n\r\n");
  for (idx = 0; idx < CNT_OF_TEST_GROUPS; idx++)
  {
    printf("Testgroup %d >> Cnt of errors: %d\r\n", idx, result[idx]);
  }
  printf("=====================================================================================\r\n\r\n");

  /* wait for user key to shutdown system */
  printf("Pres any key to exit test environment \r\n");
  getch();
  return 0;
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

