/*****************************************************************************************************************************
* @file        Stubs.c                                                                                                      *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of functionalities from the "Stubs" module.                                                   *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "stubs.h"
#include <vcruntime.h>
#include "cmocka.h"
#include "UartIf_Types.h"
#include "UartIf_PBCfg.h"
#include "UartIf.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
UartIf_ModuleConfigType uartIfModuleConfiguration[UARTIF_CNT_OF_USED_MODULES] =
{
  {&mod1RxNotFnc, &mod1RxIsrNotFnc, &mod1TxNotFnc, &mod1TxIsrNotFnc },
  {&mod2RxNotFnc, &mod2RxIsrNotFnc, &mod2TxNotFnc, &mod2TxIsrNotFnc }
};
UartIf_ReturnType uartIfBufferStatus = UARTIF_E_OK;

boolean throwTxNotificationInMcalUartTransmitFnc = FALSE;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
Std_ReturnType Det_ReportError(uint8 moduleId, uint8 errorId)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_int_equal(errorId, mock());
  return (Std_ReturnType)mock();
}

/**
* @brief           STUB >> Init the uart buffer memory with an valid configuration
* @param[]         None
* @retval          None
* @details         This function shall be called during system startup. And before calling the init and mainfuntion.
*/
void UartIf_Buffer_InitMemory(void)
{
  function_called();
}

/**
* @brief           STUB >> Request the actual RX Buffer load from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8* pActualLoad  >> Output variable
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetActualRxBufferLoad(uint8 moduleId, uint8* pActualLoad)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pActualLoad);
  *pActualLoad = (uint8)mock();
  return mock();
}

/**
* @brief           STUB >> Request the actual TX Buffer load from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8* pActualLoad  >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_BUFFER_EMPTY
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetActualTxBufferLoad(uint8 moduleId, uint8* pActualLoad)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pActualLoad);
  *pActualLoad = (uint8)mock();
  return mock();
}

/**
* @brief           STUB >> Request the actual RX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetRxReadFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pBufferFrame);
  *pBufferFrame = (UartIf_Buffer_Frame*)mock();
  return mock();
}

/**
* @brief           STUB >> Request the actual TX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame *pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_EMPTY
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_BUFFER_OVERFLOW
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetTxReadFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pBufferFrame);
  *pBufferFrame = (UartIf_Buffer_Frame*)mock();
  return mock();
}


/**
* @brief           Increment the actual TX Buffer Frame Read Element Index
* @param[in]       uint8 module ID
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_IncrementTxReadFramePointer(uint8 moduleId)
{
  function_called();
  assert_int_equal(moduleId, mock());
  return UARTIF_E_OK;
}

/**
* @brief           STUB >> Request the actual RX Buffer Frame Write Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetRxWriteFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pBufferFrame);
  *pBufferFrame = (UartIf_Buffer_Frame*)mock();
  return mock();
}

/**
* @brief           STUB >> Request the actual TX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetTxWriteFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pBufferFrame);
  *pBufferFrame = (UartIf_Buffer_Frame*)mock();
  return mock();
}

/**
* @brief           STUB >> Request the data frame size of the tx buffer from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8 *pdataSize
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetRxFrameDataSize(uint8 moduleId, uint8 *pDataSize)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pDataSize);
  *pDataSize = (uint8)mock();
  return mock();
}

/**
* @brief           STUB >> Request the data frame size of the tx buffer from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8 *pdataSize
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetTxFrameDataSize(uint8 moduleId, uint8 *pDataSize)
{
  function_called();
  assert_int_equal(moduleId, mock());
  assert_non_null(pDataSize);
  *pDataSize = (uint8)mock();
  return mock();
}

Std_ReturnType Uart_Transmit(UartIf_TxFrameType txFrame)
{
  function_called();
  assert_int_equal(txFrame.moduleID, mock());
  assert_int_equal(txFrame.cntOfData, mock());
  assert_non_null(txFrame.pData);

  if (TRUE == throwTxNotificationInMcalUartTransmitFnc)
  {
    UartIf_TxNotification(txFrame.moduleID);
  }

  return mock();
}

void mod1TxNotFnc(uint8 modId)
{
  function_called();
  modId = 0;
}

void mod1TxIsrNotFnc(uint8 modId)
{
  function_called();
  modId = 0;
}
void mod1RxNotFnc(uint8 modId)
{
  function_called();
  modId = 0;
}

void mod1RxIsrNotFnc(uint8 modId)
{
  function_called();
  modId = 0;
}

void mod2TxNotFnc(uint8 modId)
{
  function_called();
  modId = 1;
}

void mod2TxIsrNotFnc(uint8 modId)
{
  function_called();
  modId = 1;
}

void mod2RxNotFnc(uint8 modId)
{
  function_called();
  modId = 1;
}

void mod2RxIsrNotFnc(uint8 modId)
{
  function_called();
  modId = 1;
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/

