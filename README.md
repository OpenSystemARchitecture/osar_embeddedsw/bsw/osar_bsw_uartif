# OSAR_BSW_UartIf

## General
The Uart Interface Module (UartIf) is the central handler module for the Uart. It works with and Frame architecture.

## Abbreviations:
OSAR == Open System ARchitecture
UartIf = Uart Interface

## Useful Links:
- [Overall OSAR-Artifactory](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture)
- [OSAR - UartIf releases](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_UartIf/)
- [OSAR - Documents](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/)
- [OSAR - Hyperspace](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Hyperspace/)
  - Main-Tool to setup an OSAR Embedded Software Project.
- [OSAR - Tools](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/)